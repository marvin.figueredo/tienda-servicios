﻿using System;
using TiendaServicios.RabbitMQ.Bus.Eventos;

namespace TiendaServicios.RabbitMQ.Bus.Comandos
{
    public class Comando: Message
    {
        public DateTime Timestamp { get; set; }

        protected Comando()
        {
            Timestamp = DateTime.Now;
        }
    }
}
