﻿using MediatR;

namespace TiendaServicios.RabbitMQ.Bus.Eventos
{
    public class Message: IRequest<bool>
    {
        public string MessageType { get; set; }

        protected Message()
        {
            MessageType = GetType().Name;
        }
    }
}