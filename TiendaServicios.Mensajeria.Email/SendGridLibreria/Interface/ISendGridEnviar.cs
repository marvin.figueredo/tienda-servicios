﻿using System.Threading.Tasks;
using TiendaServicios.Mensajeria.Email.SendGridLibreria.Modelo;

namespace TiendaServicios.Mensajeria.Email.SendGridLibreria.Interface
{
    public interface ISendGridEnviar
    {
        Task<(bool resultado, string errorMessage)> EnviarEmail(SendGridData data);
    }
}
//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